# -*- coding: utf-8 -*-

import telebot
import config
import words
import weather
import verse
import logging

bot = telebot.TeleBot(config.telegram_token)

@bot.message_handler(content_types=["text"])
def answer(message):
    text = message.text.lower().split()
    wordset = [word for word in text if word not in words.filling]
    time = [word
        for word in wordset
        if word in words.time_words]
    city_name = '+'.join([word[0].upper() + word[1:]
        for word in wordset
        if word not in words.time_words])
    if city_name == '':
        city_name = 'Москва'
    geocode_answer = config.get_geodata(city_name)
    if geocode_answer is None:
        bot.send_message(message.chat.id, 
            "Я не знаю таких слов(")
        return
    city = geocode_answer[0]
    bot.send_message(message.chat.id, city)
    location = geocode_answer[1].split()
    time_begin, time_end = words.get_date(time)
    if time_begin is None:
        time_begin = 0
    if time_end is None:
        time_end = time_begin + 1
    if time_begin is 0 and time_end is 1:
        verse_theme, Weather = \
            weather.get_current_weather(location[1] + ',' + location[0])
        bot.send_message(message.chat.id, Weather)
        bot.send_message(message.chat.id,
            verse.get_random_verse(verse_theme))
        bot.send_photo(message.chat.id, 
                       config.get_first_image(city)) # + '+' + verse_theme))
    else:
        Weather = weather.get_weather(location[1] + ',' + location[0],
                                      time_begin,
                                      time_end)
        for day_weather in Weather:
            bot.send_message(message.chat.id, day_weather)


if __name__ == '__main__':
    logging.basicConfig(filename='stderror.log', level=logging.DEBUG)
    bot.polling(none_stop=True)

