import json
import random

weather_types = {
	'дымка',
	'дождь',
	'пасмурно',
	'снег',
	'облачно',
	'ясно',
}
def get_random_verse(weather):
	for weather_type in weather_types:
		if weather.lower().find(weather_type) != -1:
			with open(weather_type + '.txt', 'r') as library:
				catalog = json.loads(library.read())
				return random.choice(catalog)
	with open('Other.txt', 'r') as library:
		catalog = json.loads(library.read())
		return random.choice(catalog)
