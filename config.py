# -*- coding: utf-8 -*-

import json
import requests
from bs4 import BeautifulSoup

telegram_token = '460502593:AAG4gl6x7Xcq51Puaar4f2Qc_UvhTzeXD4E'
weather_key = '1fd13952da3e4c44a4e195517171512'

geo_url = 'https://geocode-maps.yandex.ru/1.x/'
def get_geodata(city_name):
    params = {
        'geocode' : city_name,
        'format' : 'json',
    }
    json_data = json.loads(requests.post(url=geo_url, data=params).text)
    try:
        name = (json_data
            ["response"]
            ["GeoObjectCollection"]
            ["featureMember"]
            [0]
            ["GeoObject"]
            ['metaDataProperty']
            ['GeocoderMetaData']
            ["text"])
        location = (json_data
            ["response"]
            ["GeoObjectCollection"]
            ["featureMember"]
            [0]
            ["GeoObject"]
            ["Point"]
            ["pos"])
        return (name, location)
    except IndexError:
        return None

def get_first_image(text):
    url = 'https://yandex.ru/images/search'
    params = {
            'text' : text,
            }
    # headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    rs = requests.post(url, data=params)
    soup = BeautifulSoup(rs.text, 'lxml')
    return soup.findAll('img', {'class' : 'serp-item__thumb'})[0]['src'][2:]