import requests
import config
import json


weather_url = 'http://api.worldweatheronline.com/premium/v1/weather.ashx'
def get_current_weather(coordinates):
    params = {
        'date': 'today',
        'format': 'json',
        'key': config.weather_key,
        'lang': 'ru',
        'num_of_days': 0,
        'tp': 12,
        'q': coordinates,
    }
    rs = requests.post(weather_url, data=params)
    weather = json.loads(rs.text)['data']['current_condition'][0]
    return weather['lang_ru'][0]['value'], current_weather_readable(weather)

def get_weather(coordinates, time_begin, time_end):
    params = {
        'format': 'json',
        'key': config.weather_key,
        'lang': 'ru',
        'num_of_days': time_end,
        'tp': 24, 
        'q': coordinates,
    }
    rs = requests.post(weather_url, data=params)
    Weather = json.loads(rs.text)['data']['weather']
    return [weather_readable(Weather, i) for i in range(time_begin, time_end)]


def current_weather_readable(weather):
    return ('Погода на сегодня:\n'
        + weather['lang_ru'][0]['value']
        + '\n'
        + 'Температура '
        + weather['temp_C']
        + '°C\n'
        + 'Давление '
        + str(int(float(weather['pressure']) * 0.750062))
        + ' мм.рт.с.')

def weather_readable(weather, i):
    return ('Погода на ' 
        + weather[i]['date']
        + ':\n'
        + weather[i]['hourly'][0]['lang_ru'][0]['value']
        + '\n'
        + 'Температура '
        + weather[i]['hourly'][0]['tempC']
        + '°C\n'
        + 'Давление '
        + str(int(int(weather[i]['hourly'][0]['pressure']) * 0.750062))
        + ' мм.рт.с.')
