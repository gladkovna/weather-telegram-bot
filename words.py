# -*- coding: utf-8 -*-

import datetime

filling = { 
            'погода',
            'погоду',
            'какая',
            'температура',
            'будет',
            'температуру',
            'бот,',
            'бот',
            'ботя,',
            'привет!',
            'привет)',
            'привет.',
            'привет,',
            'привет',
            'с',
            'в',
            'во',
            'следующие',
            'следующий',
            'следующую',
            'следующая',
            'следующих',
            'следующее',
            'эти',
            'этот',
            'это',
            'эту',
            'эта',
            'этих',
            'до',
            'скажи',
            'скажи,',
            'мне',
            'мне,',
            'пожалуйста',
            'пожалуйста,',
}

time_words = {
        'на',
        'сегодня',
        'сейчас',
        'завтра',
        'послезавтра',
        'через',
        'неделю',
        'неделе',
        'неделя',
        'сегодняшняя',
        'завтрашняя',
        'послезавтрашняя',
        'дня',
        'день',
        'дней',
        'выходные',
        'выходных',
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        'понедельник',
        'пн',
        'вторник',
        'вт',
        'среда',
        'среду',
        'ср',
        'четверг',
        'чт',
        'пятница',
        'пятницу',
        'пт',
        'суббота',
        'субботу',
        'сб',
        'воскресенье',
        'вс',
}

def closest_weekday(num):
    result = 0
    today = datetime.date.today()
    while today.weekday() != num:
        today += datetime.timedelta(1)
        result += 1
    return result

time_dict = {
        'на' : [(None, None)],
        'через' : [(None, None)],
        'дня' : [(None, None)],\
        'дней' : [(None, None)],
        'сегодня' : [(None, None)],
        'сейчас' : [(None, None)],
        'завтра' : [(1, 2)],
        'послезавтра' : [(2, 3)],
        'неделю' : [(0, 7), (7, 8)],
        'неделе' : [(0, 7)],
        'неделя' : [(0, 7)],
        'сегодняшняя' : [(None, None)],
        'завтрашняя' : [(1, None)],
        'послезавтрашняя' : [(2, None)],
        'день' : [(None, None), (1, None)],
        'выходные' : [(closest_weekday(5), closest_weekday(5) + 2), 
                      (closest_weekday(5) + 7, closest_weekday(5) + 7)],
        'выходных' : [(closest_weekday(5), None)],
        '1' : [(None, 1), (2, None)],
        '2' : [(None, 2), (3, None)],
        '3' : [(None, 3), (4, None)],
        '4' : [(None, 4), (5, None)],
        '5' : [(None, 5), (6, None)],
        '6' : [(None, 6), (7, None)],
        '7' : [(None, 7), (8, None)],
        'понедельник' : [(closest_weekday(0), None)],
        'пн': [(closest_weekday(0), None)],
        'вторник' : [(closest_weekday(1), None)],
        'вт' : [(closest_weekday(1), None)],
        'среда' : [(closest_weekday(2), None)],
        'среду' : [(closest_weekday(2), None)],
        'ср' : [(closest_weekday(2), None)],
        'четверг' : [(closest_weekday(3), None)],
        'чт' : [(closest_weekday(3), None)],
        'пятница' : [(closest_weekday(4), None)],
        'пятницу' : [(closest_weekday(4), None)],
        'пт' : [(closest_weekday(4), None)],
        'суббота' : [(closest_weekday(5), None)],
        'субботу' : [(closest_weekday(5), None)],
        'сб' : [(closest_weekday(5), None)],
        'воскресенье' : [(closest_weekday(6), None)],
        'вс' : [(closest_weekday(6), None)],
}



def get_date(time_description):
    result = (None, None)
    if 'через' in time_description:
        for item in time_dict.keys():
            if item in time_description:
                result = time_dict[item][-1]
    else:
        for item in time_dict.keys():
            if item in time_description:
                result = time_dict[item][0]
    return result


